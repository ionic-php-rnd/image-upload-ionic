import { Component, OnInit } from '@angular/core';
import { HomePage } from '../home/home.page';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-multiple-image-upload',
  templateUrl: './multiple-image-upload.page.html',
  styleUrls: ['./multiple-image-upload.page.scss'],
})
export class MultipleImageUploadPage implements OnInit {

  files=[

  ]
  constructor(public nav:NavController,public http:HttpClient) {

  }

  ngOnInit() {
  }

  selectImage(event){

    let file=event.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (e: any) => {
      this.files.push({
        url:e.target.result,
        file:file,
        status:false
      })
    }
  }
  remove(i){
    this.files.splice(i,1);
  }
  upload(){
    let param=new FormData();
    this.files.map((item,index)=>{
      param.append(`images[${index}]`,item.file)
    })
    this.http.post("http://192.168.0.142/image_upload_api/",param).subscribe((data:any)=>{
      console.log("✅",data);
      alert("Image uploaded Successfully");
      this.files=[];
    })
  }
}
