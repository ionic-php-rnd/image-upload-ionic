import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleImageUploadPage } from './multiple-image-upload.page';

describe('MultipleImageUploadPage', () => {
  let component: MultipleImageUploadPage;
  let fixture: ComponentFixture<MultipleImageUploadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleImageUploadPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleImageUploadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
