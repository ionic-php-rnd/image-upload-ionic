import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleImageUploadPage } from './single-image-upload.page';

describe('SingleImageUploadPage', () => {
  let component: SingleImageUploadPage;
  let fixture: ComponentFixture<SingleImageUploadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleImageUploadPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleImageUploadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
