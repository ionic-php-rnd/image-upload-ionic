import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-single-image-upload',
  templateUrl: './single-image-upload.page.html',
  styleUrls: ['./single-image-upload.page.scss'],
})
export class SingleImageUploadPage implements OnInit {

  image_url:any="https://lorempixel.com/200/200/";
  file:any;
  constructor(public nav:NavController,public http:HttpClient) { }

  ngOnInit() {
  }

  selectImage(event){
    this.file=event.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(this.file);
    reader.onload = (e: any) => {
      this.image_url=e.target.result;
    }
  }
  upload(){
    let param=new FormData();
    param.append("images",this.file);
    this.http.post("http://192.168.0.142/image_upload_api/",param).subscribe((data:any)=>{
      console.log("✅",data);
      if(data.status){
        alert("Image Uploaded Successfully");
      }
      else{
        alert("Something Sent Wrong");
      }
    })
  }
}
