import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SingleImageUploadPage } from './single-image-upload.page';

const routes: Routes = [
  {
    path: '',
    component: SingleImageUploadPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SingleImageUploadPage]
})
export class SingleImageUploadPageModule {}
