import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'single-image-upload', loadChildren: './single-image-upload/single-image-upload.module#SingleImageUploadPageModule' },
  { path: 'multiple-image-upload', loadChildren: './multiple-image-upload/multiple-image-upload.module#MultipleImageUploadPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
