import { Component } from '@angular/core';
import { NgForm, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  images: any = null;
  imageUrls:any=[];
  files:any=[];
  constructor(public formBuilder: FormBuilder,public http:HttpClient,public nav:NavController) {
  }
  selectImage(event) {
    this.files=event.target.files;
    let files=event.target.files;
    this.images = files;
    for(let i=0;i<files.length;i++){
      let reader = new FileReader();
      reader.readAsDataURL(files[i]);
      reader.onload = (e: any) => {
        this.imageUrls.push(e.target.result)
      }
    }
  }

  imageFile(data){
    let base64:any=[];
    let reader = new FileReader();
    reader.onload = (e: any) => {
      base64.push(e.target.result)
    }
    reader.readAsDataURL(data);

    return base64;
  }

  uploadImage(){

    let param=new FormData();

    if(this.files.length>0){
      for(let i=0;i<this.files.length;i++){
        param.append(`images[${i}]`,this.files[i]);
      }
    }


    this.http.post("http://192.168.0.142/image_upload_api/",param).subscribe(data=>{
      console.log("🐞",data);
      this.imageUrls=[]
      alert("Image Uploaded")
    })
  }
}
